import {readFileSync, writeFileSync, existsSync, mkdirSync} from 'fs'
import { basename, dirname } from 'path'
import { promisify } from 'util'
import { notify } from 'wsk'
import { render } from 'stylus'

export onEvent = (event, path, options) ->

  {input, output} = [null, null]

  try
    input = readFileSync path, encoding: 'utf8'
  catch error
    notify
      message: 'Error reading stylus file'
      value: path
      display: 'error'
      error: err

  name = basename path, '.styl'

  render input, filename: "#{name}.css", (err, str) ->
    if err?
      notify
        message: 'Error converting stylus file to css'
        value: path
        display: 'error'
        error: err
      throw err

    notify
      message: 'Converted stylus file to css'
      value: path
      display: 'compile'

    output = str

    path = path
      .replace 'assets', 'dist'
      .replace '.styl', '.css'

    unless existsSync dirname path
      mkdirSync dirname path

    try
      writeFileSync path, output
    catch err
      notify
        message: 'Error writing file to disk'
        value: path
        display: 'error'
        error: err
      throw err

    notify
      message: 'Successfully wrote css file to disk'
      value: path
      display: 'success'
