import {readFileSync, writeFileSync, existsSync} from 'fs'
import mkdirp from 'mkdirp'
makeParentsSync = mkdirp.sync
import { basename, dirname } from 'path'
import { promisify } from 'util'
import { notify } from 'wsk'

import CoffeeScript from 'coffeescript'
import Browserify from 'browserify'

export onEvent = (event, path, options) ->

  {input, output} = [null, null]

  try
    input = readFileSync path, encoding: 'utf8'
  catch error
    notify
      message: 'Error reading coffee file'
      value: path
      display: 'error'
      error: err

  try
    output = CoffeeScript.compile input,
      filename: path
      bare: yes
  catch err
    notify
      message: 'Error converting coffee file to js'
      value: path
      display: 'error'
      error: err
    throw err

  notify
    message: 'Converted coffee file to js'
    value: path
    display: 'compile'

  path = path
    .replace 'assets', 'dist'
    .replace '.coffee', '.js'

  try
    makeParentsSync dirname path
  catch err
    notify
      message: 'Failed to create directories'
      value: path
      display: 'error'

  try
    writeFileSync path, output
  catch err
    notify
      message: 'Error writing file to disk'
      value: path
      display: 'error'
      error: err
    throw err

  notify
    message: 'Successfully wrote js file to disk'
    value: path
    display: 'success'

  bundlePath = 'dist/bundle.js'

  bundler = Browserify 'index.js', basedir: 'dist/scripts'
    .transform 'babelify', presets: 'env'

  bundler.bundle (err, bundle) ->

    if err?
      notify
        message: 'Error writing bundle to disk'
        value: bundlePath
        display: 'error'
        error: err
      throw err
    else
      notify
        message: 'Successfully bundled js files'
        value: bundlePath
        display: 'compile'

    try
      writeFileSync bundlePath, bundle
    catch err
      notify
        message: 'Error writing bundle to disk'
        value: bundlePath
        display: 'error'
        error: err
      throw err

    notify
      message: 'Successfully wrote bundle to disk'
      value: bundlePath
      display: 'success'
