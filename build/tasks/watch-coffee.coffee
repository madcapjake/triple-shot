import { watcher } from 'wsk'

watchGroups = [
  {
    serviceName: 'coffee'
    path: 'assets/scripts/**/*.coffee'
    ignoreDotFiles: yes
    chokidarOptions:
      ignoreInitial: yes
    events: [
      {
        type: 'change'
        taskFiles: 'build/tasks/build-coffee.coffee'
      }
      {
        type: 'add'
        taskFiles: 'build/tasks/build-coffee.coffee'
      }
    ]
  }
]

watcher.add watchGroups, (err, watchTrees) ->
  if err?
    console.error err
    throw err
