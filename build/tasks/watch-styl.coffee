import { watcher } from 'wsk'

watchGroups = [
  {
    serviceName: 'stylus'
    path: 'assets/styles/**/*.styl'
    ignoreDotFiles: yes
    chokidarOptions:
      ignoreInitial: yes
    events: [
      {
        type: 'change'
        taskFiles: 'build/tasks/build-styl.coffee'
      }
      {
        type: 'add'
        taskFiles: 'build/tasks/build-styl.coffee'
      }
    ]
  }
]

watcher.add watchGroups, (err, watchTrees) ->
  if err?
    console.error err
    throw err
