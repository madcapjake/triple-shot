import Koa from 'koa'

app = new Koa

app.use (ctx, next) =>
  start = Date.now()
  await do next
  ms = Date.now() - start
  ctx.set 'X-Response-Time', "#{ms}ms"

app.use (ctx, next) =>
  start = Date.now()
  await do next
  ms = Date.now() - start
  console.log "#{ctx.method} #{ctx.url} - #{ms}"

app.use (ctx) =>
  ctx.body = 'Hello World'
  await

app.listen 3000
