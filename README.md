# Triple Shot

> A complete stack (front to back) web application template for CoffeeScript aficionados.

## How to use

It's not yet ready for consumption.

## Why?

You might be wondering why use CoffeeScript in an age of ES6 and ES7 but I particularly like CoffeeScript syntax and find it imminently readable. I find myself when reading a JavaScript example/tutorial, converting the example to CoffeeScript in my head. So if you're anything like me, maybe you might enjoy this.

Additionally, I thought it'd be an interesting test to see if you could write the *entire* application (back-end, front-end, and DevOps) without ever writing a line of JavaScript. My current fear is that `package.json` may need to remain a JSON file. (However, I am not opposed to engineering a solution to this!) That file aside, I think I might have found a formula that will allow it and here it is.

## Plans

Currently I am implementing the framework as a `parcel` bundle that integrates `express`, `sequelize` and a postgres database into a framework for building web applications (`v0.1.0`). The shell helper `zx` will allow me to build a CLI that preprocesses any coffee files into any necessary JavaScript versions of them. After this foundation stabilizes (`v1.0.0`), we will expand on the CLI provided via `zx` to generate the app and new parts of the app for you (`v2.0.0`). 

## Technologies

CoffeeScript for all user-modifiable files.

| Back-End     | Front-End    | DevOps       |
| --------     | -----------  | ------------ |
| express      | tonic        | parcel       |
| sequelize    | ymnnjq       | jest         |
| pg           |              | zx           |
